export type CustomInputEvent = {
  controlName: string;
  value?: string;
  isValid?: boolean;
};
