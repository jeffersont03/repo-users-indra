export type ControlValidation = {
  [key: string]: boolean;
};
