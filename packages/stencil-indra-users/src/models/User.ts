export class User {
  public firstName: string;
  public middleName: string;
  public lastName: string;
  public email: string;
  public username: string;
  public age: string;

  constructor(firstName: string = null, middleName: string = null, lastName: string = null, email: string = null, username: string = null, age: string = null) {
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    this.email = email;
    this.username = username;
    this.age = age;
  }
}

export type UserModel = {
  _id?: number;
  id?: number;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  username: string;
  age: string;
};

export type UserInfoModal = UserModel & {
  action: string;
};
