import { UserModel } from '../models/User';
import { DMService } from './data-management-service';
import axios from 'axios';

const BASE_URL = 'http://localhost:5000/api/v1/';

export class UserService {
  private dataManagementService = DMService;
  private usersRoutes = `${BASE_URL}users/`;

  async get() {
    try {
      const { data } = await axios.get(this.usersRoutes);
      const { users } = data;
      this.dataManagementService.setUserList(users);
    } catch (error) {
      this.dataManagementService.setUserList([]);
    }
  }

  async post(user: UserModel) {
    try {
      const { data } = await axios.post(this.usersRoutes, { user });
      const { user: userCreated } = data;

      this.dataManagementService.addUser(userCreated);
    } catch (error) {
      console.log(error);
    }
  }

  async put(user: UserModel) {
    const url = `${this.usersRoutes}${user._id}`;

    try {
      await axios.put(url, { user });

      this.dataManagementService.updateUser(user);
    } catch (error) {
      console.log(error);
    }
  }

  async delete(user: UserModel) {
    const url = `${this.usersRoutes}${user._id}`;

    try {
      await axios.delete(url);

      this.dataManagementService.removeUser(user);
    } catch (error) {
      console.log(error);
    }
  }
}
