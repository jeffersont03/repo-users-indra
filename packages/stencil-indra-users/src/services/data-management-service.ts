import { BehaviorSubject } from 'rxjs';
import { User, UserInfoModal, UserModel } from './../models/User';

class DataManagementService {
  private usersList: BehaviorSubject<UserModel[]> = new BehaviorSubject([]);
  private showForm: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private isOpenModal: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private userInfoModal: BehaviorSubject<UserInfoModal> = new BehaviorSubject(null);
  private userModelForm: BehaviorSubject<User> = new BehaviorSubject(new User());
  usersList$ = this.usersList.asObservable();
  showForm$ = this.showForm.asObservable();
  isOpenModal$ = this.isOpenModal.asObservable();
  userModelForm$ = this.userModelForm.asObservable();
  userInfoModal$ = this.userInfoModal.asObservable();

  changeShowForm(newValue: boolean): void {
    this.showForm.next(newValue);
  }

  changeUserModelForm(newUser: User = new User()): void {
    this.userModelForm.next(new User());

    setTimeout(() => {
      this.userModelForm.next(newUser);
    }, 100);
  }

  setUserList(users: UserModel[]) {
    this.usersList.next(users);
  }

  addUser(user: UserModel) {
    const { value } = this.usersList;
    value.push(user);

    this.changeIsOpenModal(true);
    this.changeUserInfoModal(user, 'creado');
    this.usersList.next(value);
  }

  updateUser(user: UserModel) {
    const { value } = this.usersList;
    const indexToUpdate = value.findIndex(u => u._id === user._id);

    if (indexToUpdate > -1) value.splice(indexToUpdate, 1, user);

    this.changeIsOpenModal(true);
    this.changeUserInfoModal(user, 'actualizado');
    this.usersList.next(value);
  }

  removeUser(user: UserModel) {
    const { value } = this.usersList;
    const indexToRemove = value.findIndex(u => u._id === user._id);

    if (indexToRemove > -1) value.splice(indexToRemove, 1);

    this.changeIsOpenModal(true);
    this.changeUserInfoModal(user, 'eliminado');
    this.usersList.next(value);
  }

  changeIsOpenModal(isOpen: boolean) {
    this.isOpenModal.next(isOpen);
    if (!isOpen) this.changeUserInfoModal(null);
  }

  changeUserInfoModal(user: UserModel, action = null) {
    this.userInfoModal.next({ ...user, action });
  }
}

export const DMService = new DataManagementService();
