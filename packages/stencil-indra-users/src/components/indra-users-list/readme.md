# indra-users-list



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description | Type     | Default     |
| ----------- | ------------ | ----------- | -------- | ----------- |
| `listTitle` | `list-title` |             | `string` | `undefined` |


## Dependencies

### Used by

 - [indra-users](../indra-users)

### Depends on

- [indra-button](../indra-button)
- [indra-icon](../indra-icon)

### Graph
```mermaid
graph TD;
  indra-users-list --> indra-button
  indra-users-list --> indra-icon
  indra-users --> indra-users-list
  style indra-users-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
