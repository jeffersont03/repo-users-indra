import { Component, Host, h, Prop, State } from '@stencil/core';
import { DMService } from '../../services/data-management-service';
import { UserModel } from '../../models/User';
import { UserService } from '../../services/users-service';

@Component({
  tag: 'indra-users-list',
  styleUrl: 'indra-users-list.scss',
  shadow: true,
})
export class IndraUsersList {
  @Prop() listTitle: string;
  @State() users: UserModel[] = [];
  @State() showFormLocal: boolean;
  private dataManagementService = DMService;
  private userService = new UserService();

  componentWillLoad() {
    this.getUsers();
    this.getShowForm();
  }

  getShowForm() {
    this.dataManagementService.showForm$.subscribe(show => (this.showFormLocal = show));
  }

  getUsers() {
    this.dataManagementService.usersList$.subscribe(users => {
      this.users = [...users];
    });
  }

  addUser() {
    this.dataManagementService.changeShowForm(true);
    this.dataManagementService.changeUserModelForm();
  }

  editUser(user: UserModel) {
    this.dataManagementService.changeShowForm(true);
    this.dataManagementService.changeUserModelForm(user);
  }

  removeUser(user: UserModel) {
    this.userService.delete(user);
  }

  render() {
    return (
      <Host class="indra-users-list">
        <div class="indra-users-list__header">
          <p class="indra-users-list__title">{this.listTitle}</p>
          <indra-button text="Añadir usuario" color="secondary" onClickHandler={() => this.addUser()}>
            <indra-icon iconName="user"></indra-icon>
          </indra-button>
        </div>
        <table class="indra-users-list-content">
          <thead class="indra-users-list-content__header">
            <tr>
              <th>#</th>
              <th>Nombres</th>
              <th>Apellido P.</th>
              <th>Apellido M.</th>
              <th>Email</th>
              <th>Usuario</th>
              <th>Edad</th>
              <th></th>
            </tr>
          </thead>
          {!!this.users.length && (
            <tbody class="indra-users-list-content__body">
              {this.users.map((user, i) => {
                return (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{user.firstName}</td>
                    <td>{user.middleName}</td>
                    <td>{user.lastName}</td>
                    <td>{user.email}</td>
                    <td>{user.username}</td>
                    <td>{user.age}</td>
                    <td>
                      <div class="indra-users-list-content__actions">
                        <indra-button color="primary" onClickHandler={() => this.editUser(user)}>
                          <indra-icon iconName="edit"></indra-icon>
                        </indra-button>

                        <indra-button color="error" onClickHandler={() => this.removeUser(user)}>
                          <indra-icon iconName="trash"></indra-icon>
                        </indra-button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          )}
        </table>
      </Host>
    );
  }
}
