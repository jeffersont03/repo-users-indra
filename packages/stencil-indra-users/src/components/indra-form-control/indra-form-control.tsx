import { Component, Host, h, Prop, Event, EventEmitter, State, Watch } from '@stencil/core';
import { CustomInputEvent } from '../../models/CustomInputEvent';
import { isValidControl } from '../../utils/utils';

@Component({
  tag: 'indra-form-control',
  styleUrl: 'indra-form-control.scss',
  shadow: true,
})
export class IndraFormControl {
  @Prop() disabled: boolean = false;
  @Prop() controlName: string;
  @Prop() value: string;
  @Prop() label: string;
  @Prop() type: string = 'text';
  @Event() valueChanged: EventEmitter<CustomInputEvent>;
  @State() hasError: boolean = false;
  @State() isTouched: boolean = false;
  @State() isFocused: boolean = false;
  @State() isDirty: boolean = false;

  @Watch('value')
  valueWatch() {
    // When the value of control is reseted
    if (this.value === null) {
      this.isDirty = false;
      this.isFocused = false;
      this.isTouched = false;
      this.hasError = false;
    }

    this.validateControl();
  }

  valueChangedHandler = event => {
    const { value } = event.target;
    this.valueChanged.emit({ controlName: this.controlName, value });
    this.isDirty = true;
  };

  focusHandler = () => {
    this.isTouched = true;
    this.isFocused = true;
  };

  blurHandler = () => {
    this.isFocused = false;
    this.validateControl();
  };

  validateControl() {
    this.hasError = !isValidControl(this.value, this.type);
    this.valueChanged.emit({ controlName: this.controlName, isValid: !this.hasError });
  }

  render() {
    return (
      <Host class="form-control">
        <fieldset
          class={{ 'form-control__wrapper': true, 'is-invalid': this.hasError && (this.isDirty || this.isTouched), 'is-valid': !this.hasError && (this.isTouched || this.isDirty) }}
          disabled={this.disabled}
        >
          <input
            class={{
              'form-control__input': true,
              'form-control__input--on-focus': this.isFocused,
              'form-control__input--touched': !!this.value,
            }}
            type={this.type}
            value={this.value}
            onInput={this.valueChangedHandler}
            onFocus={this.focusHandler}
            onClick={this.focusHandler}
            onBlur={this.blurHandler}
            required={true}
            autoComplete="false"
          />
          <label class="form-control__label">{this.label}</label>
        </fieldset>
      </Host>
    );
  }
}
