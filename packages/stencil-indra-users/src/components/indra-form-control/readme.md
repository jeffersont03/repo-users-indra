# indra-form-control



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type      | Default     |
| ------------- | -------------- | ----------- | --------- | ----------- |
| `controlName` | `control-name` |             | `string`  | `undefined` |
| `disabled`    | `disabled`     |             | `boolean` | `false`     |
| `label`       | `label`        |             | `string`  | `undefined` |
| `type`        | `type`         |             | `string`  | `'text'`    |
| `value`       | `value`        |             | `string`  | `undefined` |


## Events

| Event          | Description | Type                                                                       |
| -------------- | ----------- | -------------------------------------------------------------------------- |
| `valueChanged` |             | `CustomEvent<{ controlName: string; value?: string; isValid?: boolean; }>` |


## Dependencies

### Used by

 - [indra-users-form](../indra-users-form)

### Graph
```mermaid
graph TD;
  indra-users-form --> indra-form-control
  style indra-form-control fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
