import { Component, Host, h, State } from '@stencil/core';
import { DMService } from '../../services/data-management-service';
import { UserService } from '../../services/users-service';
import { UserInfoModal } from '../../components';

@Component({
  tag: 'indra-users',
  styleUrl: 'indra-users.scss',
  shadow: true,
})
export class IndraUsers {
  @State() showFormLocal: boolean;
  @State() openModal: boolean;
  @State() userInfoModal: UserInfoModal;
  private dataManagementService = DMService;
  private userService = new UserService();

  getShowForm() {
    this.dataManagementService.showForm$.subscribe(show => {
      this.showFormLocal = show;
    });
  }

  getIsOpenModal() {
    this.dataManagementService.isOpenModal$.subscribe(isOpen => {
      this.openModal = isOpen;
    });
  }

  getUserInfoModal() {
    this.dataManagementService.userInfoModal$.subscribe(userInfo => {
      this.userInfoModal = userInfo;
    });
  }

  getUsers() {
    this.userService.get();
  }

  closeModalInfoHandler = () => {
    this.dataManagementService.changeIsOpenModal(false);
  };

  componentWillLoad() {
    this.getUsers();
    this.getShowForm();
    this.getIsOpenModal();
    this.getUserInfoModal();
  }

  render() {
    return (
      <Host class="indra-users">
        <indra-users-list listTitle="Lista de usuarios"></indra-users-list>
        <indra-users-form isClosed={!this.showFormLocal}></indra-users-form>
        <indra-modal isOpen={this.openModal} userInfo={this.userInfoModal} onCloseHandler={this.closeModalInfoHandler}></indra-modal>
      </Host>
    );
  }
}
