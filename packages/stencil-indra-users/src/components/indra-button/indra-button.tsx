import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'indra-button',
  styleUrl: 'indra-button.scss',
  shadow: false,
})
export class IndraButton {
  @Prop() disabled: boolean;
  @Prop() color: string = 'primary';
  @Prop() type: string = 'button';
  @Prop() text: string;
  @Event() clickHandler: EventEmitter;

  clickButton = e => {
    e.preventDefault();
    this.clickHandler.emit();
  };

  render() {
    return (
      <button class={`indra-button indra-button--${this.color} ${this.text ?? 'indra-button--only-icon'}`} type={this.type} disabled={this.disabled} onClick={this.clickButton}>
        <slot></slot>
        {this.text && <span class="indra-button__text">{this.text}</span>}
      </button>
    );
  }
}
