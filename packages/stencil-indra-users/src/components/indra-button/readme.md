# indra-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type      | Default     |
| ---------- | ---------- | ----------- | --------- | ----------- |
| `color`    | `color`    |             | `string`  | `'primary'` |
| `disabled` | `disabled` |             | `boolean` | `undefined` |
| `text`     | `text`     |             | `string`  | `undefined` |
| `type`     | `type`     |             | `string`  | `'button'`  |


## Events

| Event          | Description | Type               |
| -------------- | ----------- | ------------------ |
| `clickHandler` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [indra-modal](../indra-modal)
 - [indra-users-form](../indra-users-form)
 - [indra-users-list](../indra-users-list)

### Graph
```mermaid
graph TD;
  indra-modal --> indra-button
  indra-users-form --> indra-button
  indra-users-list --> indra-button
  style indra-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
