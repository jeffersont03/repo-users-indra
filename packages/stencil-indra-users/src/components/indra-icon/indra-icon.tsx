import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'indra-icon',
  styleUrl: 'indra-icon.scss',
  shadow: false,
})
export class IndraIcon {
  @Prop() iconName: string;

  render() {
    return (
      <Host class="indra-icon">
        <i class={`fa fa-${this.iconName}`}></i>
      </Host>
    );
  }
}
