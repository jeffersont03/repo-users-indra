# indra-icon



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type     | Default     |
| ---------- | ----------- | ----------- | -------- | ----------- |
| `iconName` | `icon-name` |             | `string` | `undefined` |


## Dependencies

### Used by

 - [indra-modal](../indra-modal)
 - [indra-users-list](../indra-users-list)

### Graph
```mermaid
graph TD;
  indra-modal --> indra-icon
  indra-users-list --> indra-icon
  style indra-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
