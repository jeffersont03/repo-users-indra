import { Component, Host, h, Element, Prop, Event, EventEmitter } from '@stencil/core';
import { toJpeg } from 'html-to-image';
import { UserInfoModal } from '../../models/User';

@Component({
  tag: 'indra-modal',
  styleUrl: 'indra-modal.scss',
  shadow: false,
})
export class IndraModal {
  @Prop({ mutable: true }) isOpen: boolean = false;
  @Prop() userInfo: UserInfoModal;
  @Event() closeHandler: EventEmitter;
  @Element() private element: HTMLElement;

  generateImg() {
    const doc = this.element.querySelector('#img-doc') as HTMLElement;

    toJpeg(doc, { pixelRatio: 1.5 })
      .then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = 'constancia-operacion.jpeg';
        link.href = dataUrl;
        link.click();
      })
      .catch(function (error) {
        console.error('Oops, something went wrong!', error);
      });
  }

  closeModalHandler = () => {
    this.isOpen = false;
    this.closeHandler.emit();
  };

  getDate() {
    const options: Intl.DateTimeFormatOptions = {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      hour12: true,
    };

    return new Date().toLocaleTimeString('es-PE', options);
  }

  render() {
    return (
      <Host class={{ 'indra-modal-overlay': true, 'indra-modal-overlay--is-visible': this.isOpen }}>
        {this.userInfo?.action && (
          <div class="indra-modal">
            <header class="indra-modal-header">
              <p class="indra-modal-header__title">¡Usuario {this.userInfo?.action}!</p>
              <indra-button onClick={this.closeModalHandler} class="indra-modal-header__close">
                <indra-icon iconName="close"></indra-icon>
              </indra-button>
            </header>
            <section class="indra-modal-body" id="img-doc">
              <p class="indra-modal-body__title">Constancia de operación</p>
              <div class="indra-modal-body__icon-wrapper">
                <indra-icon iconName="star"></indra-icon>
              </div>
              <p class="indra-modal-body__subtitle">Usuario {this.userInfo?.action} con éxito</p>
              <section class="indra-modal-table">
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Nombre</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.firstName}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Apellido Paterno</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.middleName}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Apellido Materno</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.lastName}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Correo Electrónico</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.email}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Usuario</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.username}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Edad</p>
                  <p class="indra-modal-table-item__description">{this.userInfo.age}</p>
                </article>
                <article class="indra-modal-table-item">
                  <p class="indra-modal-table-item__title">Fecha y hora</p>
                  <p class="indra-modal-table-item__description">{this.getDate()}</p>
                </article>
              </section>
            </section>
            <footer class="indra-modal-footer">
              <indra-button text="Descargar" color="secondary" onClickHandler={() => this.generateImg()}>
                <indra-icon iconName="download"></indra-icon>
              </indra-button>
            </footer>
          </div>
        )}
      </Host>
    );
  }
}
