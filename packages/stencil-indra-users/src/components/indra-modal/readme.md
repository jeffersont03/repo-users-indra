# indra-modal



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute | Description | Type                              | Default     |
| ---------- | --------- | ----------- | --------------------------------- | ----------- |
| `isOpen`   | `is-open` |             | `boolean`                         | `false`     |
| `userInfo` | --        |             | `UserModel & { action: string; }` | `undefined` |


## Events

| Event          | Description | Type               |
| -------------- | ----------- | ------------------ |
| `closeHandler` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [indra-users](../indra-users)

### Depends on

- [indra-button](../indra-button)
- [indra-icon](../indra-icon)

### Graph
```mermaid
graph TD;
  indra-modal --> indra-button
  indra-modal --> indra-icon
  indra-users --> indra-modal
  style indra-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
