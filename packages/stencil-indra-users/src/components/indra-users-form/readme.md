# indra-users-form



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type      | Default |
| ---------- | ----------- | ----------- | --------- | ------- |
| `isClosed` | `is-closed` |             | `boolean` | `true`  |


## Dependencies

### Used by

 - [indra-users](../indra-users)

### Depends on

- [indra-form-control](../indra-form-control)
- [indra-button](../indra-button)

### Graph
```mermaid
graph TD;
  indra-users-form --> indra-form-control
  indra-users-form --> indra-button
  indra-users --> indra-users-form
  style indra-users-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
