import { Component, Host, h, Prop, State, Watch } from '@stencil/core';
import { User, UserModel } from '../../models/User';
import { ControlValidation } from '../../models/ControlValidation';
import { CustomInputEvent } from '../../components';
import { buildValidObject, isValidForm } from '../../utils/utils';
import { DMService } from '../../services/data-management-service';
import { UserService } from '../../services/users-service';

@Component({
  tag: 'indra-users-form',
  styleUrl: 'indra-users-form.scss',
  shadow: true,
})
export class IndraUsersForm {
  @Prop() isClosed: boolean = true;
  @State() userModelInitial: UserModel;
  @State() userModel: UserModel; // Local
  @State() showForm: boolean = true;
  @State() disabled: boolean = false;
  @State() isFormValid: boolean = false;
  private userModelValidates: ControlValidation;
  private dataManagementService = DMService;
  private userService = new UserService();

  @Watch('userModelInitial')
  userModelInitialChanged() {
    this.userModel = new User();

    setTimeout(() => {
      this.userModel = { ...this.userModelInitial };
    }, 100);

    this.userModelValidates = buildValidObject(this.userModel);
  }

  @Watch('isClosed')
  isClosedChanged() {
    if (this.isClosed) {
      setTimeout(() => {
        this.showForm = false;
        // Time of transition
      }, 300);
    } else {
      this.showForm = true;
    }
  }

  componentWillLoad() {
    this.getUserModelForm();
  }

  private getUserModelForm() {
    this.dataManagementService.userModelForm$.subscribe(user => {
      this.userModelInitial = user;
    });
  }

  private handleSubmit = event => {
    event.preventDefault();

    if (this.userModel._id) {
      this.userService.put(this.userModel);
    } else {
      this.userService.post(this.userModel);
    }

    this.closeForm();
  };

  private closeForm() {
    this.dataManagementService.changeShowForm(false);
  }

  private handleCancel = event => {
    event.preventDefault();
    this.closeForm();
  };

  private getValueChanged(event: CustomEvent<CustomInputEvent>) {
    const { controlName, value, isValid } = event.detail;
    if (controlName) {
      if (typeof isValid !== 'undefined') {
        this.userModelValidates = { ...this.userModelValidates, [controlName]: isValid };

        setTimeout(() => {
          this.isFormValid = isValidForm(this.userModelValidates);
        }, 0);
      } else {
        this.userModel = { ...this.userModel, [controlName]: value };
      }
    }
  }

  render() {
    return (
      <Host class={{ 'indra-users-form': true, 'indra-users-form--closed': this.isClosed }}>
        {this.showForm && (
          <form onSubmit={this.handleSubmit}>
            <div class="grid grid-cols-12">
              <div class="col-span-12">
                <indra-form-control
                  value={this.userModel.firstName}
                  label="Nombres"
                  disabled={this.disabled}
                  controlName="firstName"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-6">
                <indra-form-control
                  value={this.userModel.middleName}
                  label="Apellido Paterno"
                  disabled={this.disabled}
                  controlName="middleName"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-6">
                <indra-form-control
                  value={this.userModel.lastName}
                  label="Apellido Materno"
                  disabled={this.disabled}
                  controlName="lastName"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-10">
                <indra-form-control
                  value={this.userModel.email}
                  label="Correo Electrónico"
                  disabled={this.disabled}
                  controlName="email"
                  type="email"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-2">
                <indra-form-control
                  value={this.userModel.age}
                  label="Edad"
                  disabled={this.disabled}
                  controlName="age"
                  type="number"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-12">
                <indra-form-control
                  value={this.userModel.username}
                  label="Usuario"
                  disabled={this.disabled}
                  controlName="username"
                  onValueChanged={ev => this.getValueChanged(ev)}
                />
              </div>
              <div class="col-span-12 indra-users-form__actions">
                <indra-button color="error" text="Cancelar" type="submit" disabled={this.disabled} onClickHandler={this.handleCancel}></indra-button>
                <indra-button text="Guardar" type="submit" disabled={this.disabled || !this.isFormValid} onClickHandler={this.handleSubmit}></indra-button>
              </div>
            </div>
          </form>
        )}
      </Host>
    );
  }
}
