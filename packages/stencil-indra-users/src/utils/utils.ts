import { ControlValidation } from '../models/ControlValidation';

export function buildValidObject(object: any) {
  const keys = Object.keys(object);
  const idProps = ['id', '_id'];
  return keys.reduce((acc, curr) => {
    if (!idProps.includes(curr)) acc = { ...acc, [curr]: false };
    return acc;
  }, {});
}

export function isValidForm(object: ControlValidation) {
  return Object.values(object).every(Boolean);
}

export function isValidControl(value: string, type: string) {
  if (type === 'email') {
    const validRegex = RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
    return validRegex.test(value);
  }
  return value !== null && value !== undefined && value !== '';
}
