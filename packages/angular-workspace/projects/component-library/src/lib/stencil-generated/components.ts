/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';

import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import { Components } from 'stencil-indra-users';


@ProxyCmp({
  inputs: ['color', 'disabled', 'text', 'type']
})
@Component({
  selector: 'indra-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['color', 'disabled', 'text', 'type'],
})
export class IndraButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['clickHandler']);
  }
}


export declare interface IndraButton extends Components.IndraButton {

  clickHandler: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['controlName', 'disabled', 'label', 'type', 'value']
})
@Component({
  selector: 'indra-form-control',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['controlName', 'disabled', 'label', 'type', 'value'],
})
export class IndraFormControl {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChanged']);
  }
}


import type { CustomInputEvent as IIndraFormControlCustomInputEvent } from 'stencil-indra-users';

export declare interface IndraFormControl extends Components.IndraFormControl {

  valueChanged: EventEmitter<CustomEvent<IIndraFormControlCustomInputEvent>>;
}


@ProxyCmp({
  inputs: ['iconName']
})
@Component({
  selector: 'indra-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['iconName'],
})
export class IndraIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface IndraIcon extends Components.IndraIcon {}


@ProxyCmp({
  inputs: ['isOpen', 'userInfo']
})
@Component({
  selector: 'indra-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['isOpen', 'userInfo'],
})
export class IndraModal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['closeHandler']);
  }
}


export declare interface IndraModal extends Components.IndraModal {

  closeHandler: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
})
@Component({
  selector: 'indra-users',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class IndraUsers {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface IndraUsers extends Components.IndraUsers {}


@ProxyCmp({
  inputs: ['isClosed']
})
@Component({
  selector: 'indra-users-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['isClosed'],
})
export class IndraUsersForm {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface IndraUsersForm extends Components.IndraUsersForm {}


@ProxyCmp({
  inputs: ['listTitle']
})
@Component({
  selector: 'indra-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['listTitle'],
})
export class IndraUsersList {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface IndraUsersList extends Components.IndraUsersList {}


