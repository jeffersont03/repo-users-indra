
import * as d from './components';

export const DIRECTIVES = [
  d.IndraButton,
  d.IndraFormControl,
  d.IndraIcon,
  d.IndraModal,
  d.IndraUsers,
  d.IndraUsersForm,
  d.IndraUsersList
];
