/**
 * @openapi
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           readOnly: true
 *           example: 64e715dcd92824c42e76c4a9
 *         firstName:
 *           type: string
 *           example: Jerfferson Tejada
 *         middleName:
 *           type: string
 *           example: Tejada
 *         lastName:
 *           type: string
 *           example: Senmache
 *         email:
 *           type: string
 *           example: jeffersont03@gmail.com
 *         username:
 *           type: string
 *           example: jefryts
 *         age:
 *           type: string
 *           example: 26
 *       required:
 *         - firstName
 *         - middleName
 *         - lastName
 *         - email
 *         - username
 *         - age
 */
