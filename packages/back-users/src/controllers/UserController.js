const UserModel = require("../models/UserModel");

const getUsers = async (req, res) => {
  const users = await UserModel.find();
  res.send({ success: true, users });
};

const saveUser = async (req, res) => {
  try {
    const { user } = req.body;
    const userCreated = await UserModel.create(user);

    res.status(201).send({ success: true, user: userCreated });
  } catch (error) {
    const { errorStatus } = error;
    res
      .status(errorStatus || 500)
      .send({ success: false, message: "Something went wrong!" });
  }
};

const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { user } = req.body;

    await UserModel.findByIdAndUpdate(id, user);

    res.send({ success: true });
  } catch (error) {
    const { errorStatus } = error;
    res
      .status(errorStatus || 500)
      .send({ success: false, message: "Something went wrong!" });
  }
};

const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;

    await UserModel.findByIdAndDelete(id);

    res.send({ success: true });
  } catch (error) {
    const { errorStatus } = error;
    res
      .status(errorStatus || 500)
      .send({ success: false, message: "Something went wrong!" });
  }
};

module.exports = {
  getUsers,
  saveUser,
  updateUser,
  deleteUser,
};
