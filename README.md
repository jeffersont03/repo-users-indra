# Indra Users

## Repo con el backend / app angular / stencil microfront

Proyecto en el que se puede visualizar el mantenimiento (CRUD) de usuarios en la cual luego de cada operación (creación, actualización, eliminación) se podrá descagar una costancia.

### Tecnologías

- Backend API: Node Express
- Front: Angular + StencilJS
- BD: Mongo

## Instalación

- Para la instalación es necesario tener instalado npm
  Paso 1: Dentro de la carpeta repo-users-indra abrir una ventana de terminal y ejecutar "npm run install-build"
  Paso 2: Ejectura "npm run start:back" para iniciar el servidor del backend
  Paso 2: Ejectura "npm run start:front" para iniciar el servidor del frontend
- Para hacer uso de la aplicación acceder a "http://localhost:4200/" desde el navegador web.
- Para acceder al swagger de la API podemos acceder a "http://localhost:5000/api/v1/docs"

Eso sería todo.

Jerfferson Tejada Senmache
